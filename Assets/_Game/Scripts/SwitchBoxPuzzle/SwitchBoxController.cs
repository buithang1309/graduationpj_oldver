using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchBoxController : MonoBehaviour
{
    [SerializeField] SwitchBox[] switchBoxes;
    [SerializeField] bool isFinish;

    [SerializeField] public Material matOn;
    [SerializeField] public Material matOff;
    [SerializeField] GameObject reward;

    private void OnEnable()
    {
        ResetPuzzle();
    }
    public void CheckFinish()
    {
        isFinish = true;
        foreach(var s in switchBoxes)
        {
            if (!s.switchStatus)
            {
                isFinish = false;
            }
        }

        if (isFinish)
        {
            FinishGame();   
        }
    }

    public void ResetPuzzle()
    {
        for(int i=0; i < switchBoxes.Length; i++)
        {
            if(i==3 || i == 7)
            {
                switchBoxes[i].switchStatus = true;
                switchBoxes[i].boxMat.material = matOn;
            }
            else
            {
                switchBoxes[i].switchStatus= false;
                switchBoxes[i].boxMat.material = matOff;
            }
        }
    }

    void FinishGame()
    {
        Debug.Log("Finish game");
        reward.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
