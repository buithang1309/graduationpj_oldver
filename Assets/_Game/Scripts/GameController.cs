using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    private static GameController instance;

    public static GameController Instance { get { return instance; } }

    public GameObject playerObj;
    public PickupAndInteract pickupAndInteract;

    [Header("For Cube Puzzle")]
    public RubikController rubikController;
    //public int rubikPoint;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Start()
    {
        //rubikPoint = 0;
    }

    public void QuickDrop()
    {
        pickupAndInteract.StopClipping();
        pickupAndInteract.DropObject();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void UpdateRubikPuzzle()
    {
        rubikController.PuzzleUpdate();
    }
}
