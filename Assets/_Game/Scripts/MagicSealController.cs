using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicSealController : MonoBehaviour
{
    [SerializeField] GameObject magicSeal;
    [SerializeField] Renderer magicSealMat;
    [SerializeField] Light magicLight;
    [SerializeField] KeypadController keypad;
    Color tempValue;
    private void Start()
    {
        keypad.OnPasswordCorrect += FinishGame;
        tempValue = Color.white;
    }

    private void Update()
    {
        magicSeal.transform.Rotate(Vector3.forward * 40 * Time.deltaTime);
    }

    void FinishGame()
    {
        StartCoroutine(MagicSealDisappear());
    }

    private void OnDestroy()
    {
        keypad.OnPasswordCorrect -= FinishGame;
    }

    IEnumerator MagicSealDisappear()
    {
        float elapsedTime = 0f;

        while (elapsedTime < 5f)
        {
            float matValue = Mathf.Lerp(1, 0, elapsedTime / 5f);
            float lightValue = Mathf.Lerp(5, 0, elapsedTime / 5f);
            tempValue.a = matValue;
            magicSealMat.material.color = tempValue;
            magicLight.intensity = lightValue;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        this.gameObject.SetActive(false);
    }
}
