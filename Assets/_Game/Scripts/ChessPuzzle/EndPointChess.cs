using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPointChess : MonoBehaviour
{
    [SerializeField] ChessPuzzleController chessPuzzleController;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("FinishGame");
            chessPuzzleController.FinishGame();
            this.gameObject.SetActive(false);
        }
    }
}
