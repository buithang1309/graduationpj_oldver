using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPuzzleController : MonoBehaviour
{
    [SerializeField] private GameObject[] bullseye;
    [SerializeField] GameObject handGun;
    public int pointCount;

    private void Start()
    {
        pointCount = 0;
        StartCoroutine(Spawner());
    }
    private void Update()
    {
        if(pointCount >= 10)
        {
            PuzzleFinish();
        }
    }

    private IEnumerator Spawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            for (int i = 0; i < bullseye.Length; i++)
            {
                bullseye[i].SetActive(true);
            }
            yield return new WaitForSeconds(2f);
            for (int i = 0; i < bullseye.Length; i++)
            {
                bullseye[i].SetActive(false);
                bullseye[i].transform.position = new Vector3(Random.Range(-4, 5), 3f, Random.Range(-4, 5));
            }
        }
        
    }

    void PuzzleFinish()
    {
        this.gameObject.SetActive(false);
    }
}
