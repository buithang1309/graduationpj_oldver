using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BullseyeController : MonoBehaviour
{
    [SerializeField] GunPuzzleController gunPuzzleController;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<BulletController>())
        {
            gunPuzzleController.pointCount++;
            this.gameObject.SetActive(false);
        }
    }
}
