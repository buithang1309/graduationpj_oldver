using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CanPickUp : MonoBehaviour
{
    public bool isSwitchBox;
    public bool isClock;
    public bool isRubik;
    public SnapPoint snapPoint;
    public bool canHoldHand;
    //for vr
    public XROffsetGrabbable xrGrab;
    public XRSimpleInteractable xrInteract;

    public void EnableComponent()
    {
        if (isSwitchBox)
        {
            var switchBox = GetComponent<SwitchBox>();
            switchBox.enabled = true;
            switchBox.EnableSwitchBox();
        }
        if (isClock)
        {
            var clock = GetComponent<ClockInteract>();
            clock.enabled = true;
            clock.EnableClock();
        }
        if (isRubik)
        {
            GameController.Instance.UpdateRubikPuzzle();
        }
    }

    private void OnDisable()
    {
        if (xrGrab!=null)
        {
            xrGrab.enabled = false;
            this.GetComponent<Rigidbody>().isKinematic = true;
            if (xrInteract != null)
            {
                xrInteract.enabled = true;
            }
        }
        else
        {
            GameController.Instance.QuickDrop();
        }
    }
}
