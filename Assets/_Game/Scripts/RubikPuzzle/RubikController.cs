using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubikController : MonoBehaviour
{
    [SerializeField] public int coreCount;
    [SerializeField] EndGameController endGame;
    [SerializeField] public int cubeCount;
    
    void Start()
    {
        coreCount = 0;
        cubeCount = 1;
    }

    private void Update()
    {
        if(coreCount >= 27 || cubeCount ==6)
        {
            PuzzleFinish();
        }
    }

    void PuzzleFinish()
    {
        Debug.Log("Finish");
        this.gameObject.SetActive(false);
        endGame.gameObject.SetActive(true); 
        endGame.Winning();
    }

    public void PuzzleUpdate()
    {
        cubeCount++;
    }
}
